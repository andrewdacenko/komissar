var express = require('express');
var router = express.Router();
var async = require('async');
var ftpHelper = require('../lib/ftpHelper');
var adbkHelper = require('../lib/adbkHelper');

/* GET home page. */
router.get('/', function(req, res) {
    async.parallel(
        [
            ftpHelper.getListOfADBKs,
            ftpHelper.getNames
        ],
        function(err, result) {
            if (err) throw err;

            var list = result[0];
            var names = result[1];

            // console.log(result);

            res.render('index_2', {
                list: list,
                names: names
            });
        }
    );
});

router.post('/', function(req, res) {
    var adbk = req.body.adbk;
    var date_end = req.body.date_end;
    var date_start = req.body.date_start;

    if (!adbk || !date_end || !date_start) {
        res.send(404);
    }

    async.parallel(
        [

            function(callback) {
                require('fs').readFile(__dirname + '/../Basa.json', function(err, data) {
                    if (err) throw err;

                    var persons = JSON.parse(data);
                    callback(null, persons);
                });
            },
            function(waterfallCallback) {
                async.waterfall(
                    [
                        ftpHelper.getListOfADBKs,
                        function(list, callback) {
                            callback(null, list, adbk, date_start, date_end);
                        },
                        adbkHelper.findIncass
                    ], function(err, result) {
                        if (err) throw err;

                        waterfallCallback(null, result);
                    }
                );
            }
        ],
        function(err, result) {
            if (err) throw err;
            res.render('index_result_2', {
                persons: result[0],
                results: result[1]
            });
        }
    );


});

module.exports = router;