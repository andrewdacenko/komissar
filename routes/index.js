var express = require('express');
var router = express.Router();
var async = require('async');
var ftpHelper = require('../lib/ftpHelper');
var adbkHelper = require('../lib/adbkHelper');

/* GET home page. */
router.get('/', function(req, res) {
    async.parallel(
        [
            ftpHelper.getListOfADBKs,
            ftpHelper.getNames
        ],
        function(err, result) {
            if (err) throw err;

            var list = result[0];
            var names = result[1];

            // console.log(result);

            res.render('index', {
                list: list,
                names: names
            });
        }
    );
});

router.post('/', function(req, res) {
    var card = req.body.card;
    var adbk = req.body.adbk;

    if (!adbk || !card) {
        res.send(404);
    }

    var events = {
        212: 'Операція з гаманцем',
        1: 'Включення живлення',
        2: 'Вхід до технологічного меню',
        3: 'Завершення роботи',
        4: 'Вхід до меню інкасування',
        5: 'Перегляд історії',
        6: 'Відкриття пристрою (спрацював датчик )',
        7: 'Закриття пристрою (спрацював датчик )',
        8: 'Вилучення бункеру',
        9: 'Встановлення бункеру',
        15: 'Додана купюра)',
        17: 'Запуск ПЗ(No версії',
        18: 'Встановлена картка',
        19: 'Завершення роботи з карткою',
        28: 'Купюру повернуто',
        29: 'Прийнято купюру',
        39: 'Друк службового чеку',
        42: 'Обслуговування пристрою',
        43: 'Закінчення обслуговування',
        45: 'Перевірка бази даних',
        231: 'Інкасація',
        206: 'Запис ресурсу',
    };

    var errors = {
        0: 'помилка вiдсутня',
        1: 'недостатньо ресурсiв',
        2: 'невiрний срок дiї',
        3: 'стороння картка',
        4: 'помилка вiдсутня',
        5: 'помилка вiдсутня',
    };


    async.waterfall(
        [

            ftpHelper.getListOfADBKs,
            function(list, callback) {
                callback(null, list, card, adbk);
            },
            adbkHelper.findCard
        ], function(err, result) {
            if (err) throw err;

            // 0000 0060 0100 2143

            res.render('index_result', {
                results: result,
                events: events,
                errors: errors
            });
        }
    );
});

module.exports = router;