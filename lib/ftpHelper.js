var ftp = require('ftp');
var fs = require('fs');
var ftpHelper = {};

ftpHelper.getListOfADBKs = function(callback) {
    var c = new ftp();

    c.on('ready', function() {
        c.list('ADBK_dump20130225', function(err, list) {
            if (err) throw err;
            console.log('done list');

            var data = {};

            for (var i = 0; i < list.length; i++) {
                var key = list[i].name.substr(5, 5);
                var iter = list[i].name.substr(11, 2);

                if (!data[key]) {
                    data[key] = [];
                }

                data[key].push(iter);
            };

            c.end();
            callback(null, data);
        });
    });

    c.connect({
        host: "avms.dit.in.ua",
        user: "student",
        password: "student"
    });
}

ftpHelper.getNames = function(callback) {
    var c = new ftp();

    c.on('ready', function() {
        c.get('nodes.name', function(err, stream) {
            if (err) throw err;
            var string = '';

            stream.on('data', function(data) {
                string += data;
            });

            stream.on('end', function() {
                var names = string.split("\n");
                var data = {};
                for (var i = 0; i < names.length; i++) {
                    var trimmed = names[i].trim();
                    if (trimmed !== '') {
                        var a = trimmed.split(';');
                        data[a[0]] = a[1];
                    }
                };
                console.log('done names');
                c.end();
                callback(null, data);
            });
        });
    });

    c.connect({
        host: "avms.dit.in.ua",
        user: "student",
        password: "student"
    });
}

ftpHelper.downloadRar = function(item, adbk, callback) {
    var c = new ftp();

    var name = 'adbk.' + adbk + '.' + item + '.rar';

    c.on('ready', function() {
        c.get('ADBK_dump20130225/' + name, function(err, stream) {
            if (err) throw err;

            stream.once('close', function() {
                callback(null, item, adbk);
                c.end();
            });

            stream.pipe(fs.createWriteStream(__dirname + '/../tmp/' + name));
        });
    });


    c.connect({
        host: "avms.dit.in.ua",
        user: "student",
        password: "student"
    });
}

module.exports = ftpHelper;