var fs = require('fs');
var async = require('async');
var sqlite3 = require('sqlite3');
var rarfile = require('rarfile');
var ftpHelper = require('./ftpHelper');
var adbkHelper = {};

adbkHelper.findCard = function(list, card, adbk, globalCallback) {

    var files = list[adbk];
    var result = [];

    // load file
    // unrar file
    // sql select

    async.each(files, function(item, eachCallback) {

        console.log('item = ' + item);

        async.waterfall(
            [

                function(callback) {
                    callback(null, item, adbk);
                },
                ftpHelper.downloadRar,
                adbkHelper.unrarFile,
                function(item, adbk, callback) {
                    callback(null, card, item, adbk);
                },
                adbkHelper.findEventsByCard
            ],
            function(err, rows) {
                if (err) throw err;

                result.push({
                    fileName: 'adbk.' + adbk + '.' + item + '.rar',
                    adbk: adbk,
                    rows: rows
                });

                eachCallback();
            }
        );

    }, function(err) {
        if (err) throw err;

        globalCallback(null, result);
    });
}

adbkHelper.findIncass = function(list, adbk, date_start, date_end, globalCallback) {
    var files = list[adbk];
    var result = [];

    async.each(files, function(item, eachCallback) {

        console.log('item = ' + item);

        async.waterfall(
            [

                function(callback) {
                    callback(null, item, adbk);
                },
                ftpHelper.downloadRar,
                adbkHelper.unrarFile,
                function(item, adbk, callback) {
                    callback(null, item, adbk, date_start, date_end);
                },
                adbkHelper.sqlIncass
            ],
            function(err, rows) {
                if (err) throw err;

                result.push({
                    fileName: 'adbk.' + adbk + '.' + item + '.rar',
                    adbk: adbk,
                    rows: rows
                });

                eachCallback();
            }
        );

    }, function(err) {
        if (err) throw err;

        globalCallback(null, result);
    });
}

adbkHelper.unrarFile = function(item, adbk, callback) {
    var name = 'adbk.' + adbk + '.' + item + '.rar';
    var sql = 'adbk.' + adbk + '.' + item + '.db3';
    var rf = new rarfile.RarFile(__dirname + '/../tmp/' + name);

    console.log(item + ' start pipe');
    var outfile = fs.createWriteStream(__dirname + '/../tmp/db3/' + sql);

    outfile.on('finish', function() {
        callback(null, item, adbk);
    });

    rf.pipe('ADBK/Base/_adbkDB_.db3', outfile);
}

adbkHelper.findEventsByCard = function(card, item, adbk, callback) {
    console.log('findEventsByCard');
    var sql = 'adbk.' + adbk + '.' + item + '.db3';
    var db = new sqlite3.Database(__dirname + '/../tmp/db3/' + sql);

    var rows = [];

    db.serialize(function() {
        db.all("SELECT * FROM events LIMIT 5", function(err, rows) {
            // console.log(rows);
            // rows.push(rows);
            callback(null, rows);
        });
    });

    db.close();
}

adbkHelper.sqlIncass = function(item, adbk, date_start, date_end, callback) {
    console.log('sqlIncass');
    date_end = date_end.replace(/(\d{2})\/(\d{2})\/(\d{4})/, '$3$2$1' + '000000');
    date_start = date_start.replace(/(\d{2})\/(\d{2})\/(\d{4})/, '$3$2$1' + '000000');
    console.log(date_end, date_start);
    var sql = 'adbk.' + adbk + '.' + item + '.db3';
    var db = new sqlite3.Database(__dirname + '/../tmp/db3/' + sql);

    var rows = [];

    db.serialize(function() {
        db.all("SELECT * FROM events WHERE EventCode = 231 AND DateTime BETWEEN ? AND ?", [date_start, date_end], function(err, rows) {
            // console.log(rows);
            // rows.push(rows);
            callback(null, rows);
        });
    });

    db.close();
}


module.exports = adbkHelper;