var XLS = require('xlsjs');
var xls = XLS.readFile(__dirname + '/../Basa.xls');

var sheet = xls.Sheets[xls.SheetNames[0]];

var result = {};
result.names = [];
result.cards = [];

var name = '';
var card = '';

var range = sheet['!range'].e.r;

for (var i = 1; i <= range; i++) {

    card = sheet['F' + i];

    if (card) {
        card = card.v.toString().replace(/(\d{2})(\d{4})(\d{4})/, '0000 00$1 $2 $3');

        var names = [];
        var fname = sheet['B' + i] ? sheet['B' + i].v : '';
        var lname = sheet['C' + i] ? sheet['C' + i].v : '';
        var mname = sheet['D' + i] ? sheet['D' + i].v : '';
        names.push(fname);
        names.push(lname);
        names.push(mname);

        name = names.join(' ').trim();

        if (name === '') {
            name = card;
        }

        result.names.push(name);
        result.cards.push(card);
    }

};

require('fs').writeFile(__dirname + '/../Basa.json', JSON.stringify(result, null, 4), function(err) {
    if (err) throw err;
    console.log('It\'s saved!');
});